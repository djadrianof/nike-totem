FROM mangar/fountainjs:1.0

MAINTAINER Marcio Mangar "marcio.mangar@gmail.com"


RUN mkdir -p /app 
ADD ./ /app
WORKDIR /app


RUN rm /etc/nginx/nginx.conf
COPY ./_extra/nginx.conf /etc/nginx


EXPOSE 80

CMD ["nginx", "-g", "daemon off;"]