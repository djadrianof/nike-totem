import React, { Component } from 'react';
import classNames from 'classnames';

import Gallery from '../containers/sections/gallery';
import Products from '../containers/sections/products';

export default class InternalComponent extends Component {
  constructor(props) {
    super(props);
  }

  productsComponent() {
    if( this.props.selected.products ) {
      return <Products />;
    }
  }

  render() {
    return (
      <div>
        { this.productsComponent() }
      </div>
    );
  }
}
