import React, { PropTypes, Component } from 'react';
import classNames from 'classnames';
import Slider from 'react-slick';
import { STATE } from '../../helpers/constants';
import GalleryDetails from '../../containers/sections/gallery-details';
import Close from '../../components/close';

import TweenMax from 'gsap';

export default class GalleryDetailsModalComponent extends Component {
  constructor(props) {
    super(props);
    this.open = false;
  }

  componentDidUpdate() {
    // this.initializeHammer();
  }

  initializeHammer() {
    if( !this.hammer ) {
      let galleryElement = document.querySelector('.nkTotem-Gallery-Details-Modal');
      this.hammer = new Hammer(galleryElement);

      this.hammer.get('swipe').set({
        direction: Hammer.DIRECTION_VERTICAL
      });

      this.hammer.on("swipedown", (evt) => {
        this.props.closeGalleryDetailModal();
        evt.srcEvent.preventDefault();
      });
    }
  }

  sliderProps() {
    return {
      infinite      : true,
      speed         : 400,
      slidesToShow  : 1,
      slidesToScroll: 1,
      swipe         : false,
      draggable     : false,
      easing        : 'easeInOutBack',
      slickGoTo     : parseInt(this.props.detail) || 0,
      afterChange   : (evt) => {
        if(this.props.open) {
          this.props.changeGalleryDetailModal(evt);
        }
      }
    };
  }

  modalGalleryDetailsTemplate() {
    var photo = document.querySelectorAll(".nkTotem-Gallery-Details-Modal");

    if( this.props.open ) {
      TweenMax.to(photo, 1.2, { y: "0%", ease:"Power2.easeInOut", delay: 0.3});

      this.open = true;
    }

    if( !this.props.open ) {
      TweenMax.to(photo, 1.5, { y: "100%", ease:"Power2.easeInOut", delay: 0.6});
    }

    if( this.props.section ) {
      let getGalleryDetailsModalImage;

      return (
        <Slider {...this.sliderProps()}>
          {
            this.props.images.details.map((item, i) => {
              getGalleryDetailsModalImage = require(`../../../../assets/images/${this.props.section}/details/${item.image}`);

              return (
                <figure key={i} className="nkTotem-Gallery-Details-Modal-Figure">
                  <img src={getGalleryDetailsModalImage} />
                  <figcaption className="nkTotem-Gallery-Details-Modal-Info">
                    <h2 className="nkTotem-Gallery-Details-Modal-Title u-t-futura-medium u-t-uppercase">{item[this.props.language].title} {this.props.open}</h2>
                    <p className="u-t-tradegothic-big u-t-uppercase">{item[this.props.language].description}</p>
                  </figcaption>
                </figure>
              )
            })
          }
        </Slider>
      );
    }
  }

  render() {
    return (
      <div className="nkTotem-Gallery-Details-Modal">
        { this.modalGalleryDetailsTemplate() }
        <Close onClick={this.props.closeGalleryDetailModal.bind(this)} id="galleryModalButton" delayShow="1.5" doHide={`${ this.props.open ? 0 : 1 }`} />
        <GalleryDetails />
      </div>
    )
  }
}
