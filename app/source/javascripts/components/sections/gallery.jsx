import React, { PropTypes, Component } from 'react';
import classNames from 'classnames';
import Slider from 'react-slick';
import Hammer from 'hammerjs';
import { STATE } from '../../helpers/constants';
import GalleryDetailsModal from '../../containers/sections/gallery-details-modal';
import Video from '../../components/video';
import Close from '../../components/close';
import StoresModalComponent from '../../components/stores-modal';

import TweenMax from 'gsap';

export default class GalleryComponent extends Component {
  constructor(props) {
    super(props);

    this.productsElements = {
      container: '.nkTotem-Gallery',
      title: '.nkTotem-Gallery-Title',
      text: '.nkTotem-Gallery-Text',
      button: '.nkTotem-Gallery-Button'
    }

    this.opened = false;
    this.hammer = null;
  }

  componentDidUpdate() {
    if( !this.hammer ) {
      // this.initializeHammer();
    }

    if( this.props.open ) {
      this.animateOpen();
    }

    if( !this.props.open ) {
      this.animateClose();
    }
  }

  initializeHammer() {
    let galleryElement = document.querySelector('.nkTotem-Gallery');
    this.hammer = new Hammer(galleryElement);

    this.hammer.get('swipe').set({
      direction: Hammer.DIRECTION_VERTICAL
    });

    this.hammer.on("swipedown", (evt) => {
      this.props.closeGallery();
      evt.srcEvent.preventDefault();
    });

    this.hammer.on("swipeup", (evt) => {
      this.props.openGalleryDetailsModal();
      evt.srcEvent.preventDefault();
    });
  }

  animateOpen() {
    if( !this.opened ) {
      TweenMax.to(this.productsElements.container, 1.2, { y: "0%", ease:"Power2.easeInOut", delay: 0.2});
      TweenMax.to(this.productsElements.title, 0.85, { y: 0, opacity: 1, ease:"Power2.easeInOut", delay: 0.85 });
      TweenMax.to(this.productsElements.text, 0.85, { y: 0, opacity: 1, ease:"Power2.easeInOut", delay: 1.05 });
      TweenMax.to(this.productsElements.button, 0.85, { scale: 1, ease: Back.easeOut.config(1.6), delay: 1.7 });

      if( this.refs.videoGallery ) {
        this.refs.videoGallery.load();
        this.refs.videoGallery.play();
      }

      this.opened = !this.opened;
    }
  }

  animateClose() {
    if( this.opened ) {
      TweenMax.to(this.productsElements.container, 1.5, { y: "100%", ease:"Power2.easeInOut", delay: 0.4 });
      TweenMax.to(this.productsElements.title, 0.8, { y: "100px", opacity: 0, ease:"Power2.easeInOut", delay: 1.1 });
      TweenMax.to(this.productsElements.text, 0.8, { y: "100px", opacity: 0, ease:"Power2.easeInOut", delay: 1.3 });
      TweenMax.to(this.productsElements.button, 0.7, { scale: 0, ease: Back.easeOut.config(1.6), delay: 1.3 });

      this.opened = !this.opened;
    }
  }

  openStoreModal(){
    return true
  }

  galleryTemplate() {
    if( this.props.data ) {
      let videoGallery = require(`../../../../assets/videos/${this.props.section}/${this.props.data.video}`);
      let videoFrame = require(`../../../../assets/images/${this.props.section}/video-frame/${this.props.data['video-frame']}`);

      return (
        <div>
          <aside className="nkTotem-Gallery-Info u-Letterings-Vertical">
            <h2 className="nkTotem-Gallery-Title u-t-futura-medium u-t-uppercase u-c-white">{this.props.data[this.props.language].title}</h2>
            <span className="nkTotem-Gallery-Text u-t-tradegothic-big u-t-uppercase u-c-white u-d-block u-mt-20">{this.props.data[this.props.language].description }</span>
            <button className="nkTotem-Gallery-Button u-button u-mt-40" onClick={() => { this.props.openGalleryDetailsModal(); }}>{STATE.INITIAL.common['learn-more'][this.props.language]}</button>
            <button className="nkTotem-Gallery-Button u-button u-mt-40" onClick={() => { this.props.openGalleryDetailsModal(); }}>{STATE.INITIAL.common['buy'][this.props.language]}</button>
          </aside>
          <div className="nkTotem-Gallery-Product">
            <Video id="gallery" video={videoGallery} frame={videoFrame} pause={`${ this.opened ? 1 : 0 }`} autoplay="1" delayPause="1000" />
          </div>
          <StoresModalComponent />
        </div>
      );
    }
  }

  render() {
    return (
      <div>
        <div className="nkTotem-Gallery">
          { this.galleryTemplate() }
          <Close onClick={this.props.closeGallery.bind(this)} id="galleryButton" delayShow="1.5" doHide={`${ this.props.open ? 0 : 1 }`} />
        </div>
      </div>
    )
  }
}
