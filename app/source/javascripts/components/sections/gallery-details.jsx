import React, { PropTypes, Component } from 'react';
import classNames from 'classnames';
import Slider from 'react-slick';
import { STATE } from '../../helpers/constants';

import TweenMax from 'gsap';

export default class GalleryDetailsComponent extends Component {
  constructor(props) {
    super(props);

    this.galleryDetailsElements = {
      container: '.nkTotem-Gallery-Details'
    }

    this.opened = false;
  }

  getColumnsPercent() {
    let productDetails = this.props.images.details;

    return {
      flex: `0 0 ${100 / productDetails.length}%`
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if( this.props.modalOpened ) this.animateOpen();
    if( !this.props.modalOpened ) this.animateClose();

    this.currentElement && this.currentElement.classList.remove('current');
    this.currentElement = null;
  }

  animateOpen() {
    if( !this.opened ) {
      TweenMax.to(this.galleryDetailsElements.container, 1.2, { y : "0%", ease:"Power2.easeInOut", delay: 0.8 });

      this.opened = !this.opened;
    }
  }

  animateClose() {
    if( this.opened ) {
      TweenMax.to(this.galleryDetailsElements.container, 1.2, { y : "100%", ease:"Power2.easeInOut", delay: 0.2 });

      this.opened = false;
    }
  }

  get previousElement(){
    return this._previousElement;
  }

  set previousElement(value){
    this._previousElement = value;
  }

  get currentElement(){
    return this._currentElement;
  }

  set currentElement(value){
    this._currentElement = value;
  }

  onItemClick(evt){
    this.props.showDetail(evt);
  }

  galleryDetailsTemplate() {
    if( this.props.section ) {
      let getGalleryDetailsImage;

      return(
        <ul className="nkTotem-Gallery-Details">
          {
            this.props.images.details.map((item, i) => {
              getGalleryDetailsImage = require(`../../../../assets/images/${this.props.section}/details/${item.thumb}`);

              return (
                <li key={i} style={this.getColumnsPercent()} className={`${this.props.detail == i ? 'current' : ''}`} data-image-index={i} onClick={this.onItemClick.bind(this)}>
                  <figure>
                    <img src={getGalleryDetailsImage} />
                  </figure>
                </li>
              )
            })
          }
        </ul>
      );
    }
  }

  render() {
    return (
      <div>
        { this.galleryDetailsTemplate() }
      </div>
    )
  }
}
