import React from 'react';
import ReactDOM from 'react-dom';
import classNames from 'classnames';
import Hammer from 'hammerjs';
import Slider from 'react-slick';
import { STATE } from '../../helpers/constants';
import ProductsMenu from '../../containers/sections/products-menu';
import Gallery from '../../containers/sections/gallery';
import Close from '../../components/close';
import StoresModalComponent from '../../containers/stores-modal';
import GalleryDetailsModal from '../../containers/sections/gallery-details-modal';

import TweenMax from 'gsap';

export default class ProductsComponent extends React.Component {
  constructor(props) {
    super(props);

    this.productsElements = {
      title      : ' .slick-active h2',
      description: '.slick-active span',
      button     : '.nkTotem-Products-Button',
      container  : '.nkTotem-Products'
    }

    this.currentId = 0;
  }

  initializeHammer() {
    if( !this.hammer ) {
      let productsItemElement = document.querySelector('.nkTotem-Products-Carousel');
      this.hammer = new Hammer(productsItemElement);

      this.hammer.get('swipe').set({
        direction: Hammer.DIRECTION_VERTICAL
      });

      this.hammer.on("swipeup", (evt) => {
        this.props.openGallery();
        evt.srcEvent.preventDefault();
      });

      this.hammer.on("swipedown", (evt) => {
        this.props.closeGallery();
        evt.srcEvent.preventDefault();
      });
    }
  }

  componentDidUpdate() {}

  sliderProps() {
    return {
      infinite      : false,
      speed         : 400,
      slidesToShow  : 1,
      slidesToScroll: 1,
      draggable     : false,
      easing        : 'easeInOutBack',
      swipe         : false,
      slickGoTo     : parseInt(this.props.slideUpdated),
      beforeChange : (evt) => {
        TweenMax.to(this.productsElements.title, 0, { y: "30px", opacity: 0, ease:"Power2.easeOut" });
        TweenMax.to(this.productsElements.description, 0, { y: "40px", opacity: 0, ease:"Power2.easeOut" });
        TweenMax.to(this.productsElements.button, 0, { scale: 0, ease:"Power2.easeOut" });

        for( let ref in this.refs ){
          this.refs[ref].currentTime = 0
        }
      },
      afterChange  : (evt) => {
        this.props.changedSlide( evt );

        TweenMax.to(this.productsElements.title, 0.8, { y: 0, opacity: 1, ease:"Power2.easeInOut"}, 0);
        TweenMax.to(this.productsElements.description, 0.8, { y: 0, opacity: 1, ease:"Power2.easeInOut", delay: 0.3}, 0);
        TweenMax.to(this.productsElements.button, 0.7, { scale: 1, ease: Back.easeOut.config(1.6), delay: 0.8}, 0);

        this.refs[`videoProduct${evt}`].play();
      }
    };
  }

  animateOpen() {
    if( !this.opened ) {
      TweenMax.to(this.productsElements.container, 1.2, { y: "0%", ease:"Power2.easeInOut", delay: 0.3 });
      TweenMax.to(this.productsElements.title, 0.8, { y: 0, opacity: 1, ease:"Power2.easeInOut", delay: 1 });
      TweenMax.to(this.productsElements.description, 0.8, { y: 0, opacity: 1, ease:"Power2.easeInOut", delay: 1.2 });
      TweenMax.to(this.productsElements.button, 0.7, { scale: 1, ease: Back.easeOut.config(1.6), delay: 1.8 });

      this.opened = !this.opened;
      this.refs[`videoProduct0`].load();
      this.refs[`videoProduct0`].play();
    }
  }

  animateClose() {
    if( this.opened ) {
      TweenMax.to(this.productsElements.container, 1.5, { y: "100%", ease:"Power2.easeInOut", delay: 0.3 });
      TweenMax.to(this.productsElements.title, 1.2, { y: "30px", opacity: 0, ease:"Power2.easeOut", delay: 1.2 });
      TweenMax.to(this.productsElements.description, 1.2, { y: "40px", opacity: 0, ease:"Power2.easeOut", delay: 1.2 });
      TweenMax.to(this.productsElements.button, 1.2, { scale: 0, ease: Back.easeOut.config(1.8), delay: 1.2 });
      this.opened = !this.opened;

      for( let ref in this.refs ){
        this.refs[ref].pause();
      }
    }
  }

  sourceVideo(videoProduct) {
    if(this.props.open) {
      return <source src={videoProduct} type="video/webm" />
    }
  }

  productsTemplate() {
    if( this.props.open ) {
      this.animateOpen();
    }

    if( !this.props.open ) {
      this.animateClose();
    }

    if( this.props.section.products ) {
      let getProductsImage;
      let videoProduct;

      return(
        <div>
          <Slider {...this.sliderProps()} className="nkTotem-Products-Carousel">
            {
              this.props.section.products.map((item, i) => {
                getProductsImage = require(`../../../../assets/images/${this.props.section.name}/video-frame/${item['video-frame']}`);
                videoProduct = require(`../../../../assets/videos/${this.props.section.name}/${item.video}`);

                return (
                  <div key={i} className="nkTotem-Products-Video">
                    <div>
                      <video loop poster={getProductsImage} id="foo" key={i} ref={`videoProduct${i}`}>
                        { this.sourceVideo(videoProduct) }
                      </video>
                    </div>
                    <div className="nkTotem-Products-Info">
                      <h2 className="nkTotem-Products-Info-Title u-t-futura-medium u-t-uppercase">
                        {item[this.props.language].title}
                      </h2>
                      <span className="nkTotem-Products-Info-Desc u-t-tradegothic-big u-t-uppercase">
                        {item[this.props.language].description}
                      </span>
                      <button className="nkTotem-Products-Button u-button u-mt-40 u-mr-20" onClick={() => { this.props.openGallery(); }}>{STATE.INITIAL.common['view'][this.props.language]}</button>
                      <button className="nkTotem-Products-Button u-button u-mt-40" onClick={() => { this.props.openStores(); }}>{STATE.INITIAL.common['buy'][this.props.language]}</button>
                    </div>
                  </div>
                )
              })
            }
          </Slider>
        </div>
      )
    }
  }

  productMenuTemplate() {
    if(this.props.section.products) {
      return <ProductsMenu />;
    }
  }

  render() {
    return (
      <div className="nkTotem-Products">
        { this.productMenuTemplate() }
        { this.productsTemplate() }
        <Close onClick={this.props.closeProducts.bind(this)} id="productsButton" delayShow="1.5" doHide={`${ this.props.open ? 0 : 1 }`} />
        <StoresModalComponent />
        <GalleryDetailsModal />
      </div>
    )
  }
}
