import React, { PropTypes, Component } from 'react';
import classNames from 'classnames';
import { STATE } from '../../helpers/constants';

import TweenMax from 'gsap';

export default class ProductsMenuComponent extends Component {
  constructor(props) {
    super(props);
  }

  animateOpen() {
    TweenMax.staggerTo(".nkTotem-Menu-Container-Item--Products", 0.6, { y:0, ease:"Power2.easeInOut", delay: 0.9 }, 0.2);
  }

  animateClose() {
    TweenMax.staggerTo(".nkTotem-Menu-Container-Item--Products", 0.6, { y: "100px", ease:"Power2.easeInOut", delay: 1.2 });
  }

  productsMenuTemplate() {
    if(this.props.productOpened) {
      this.animateOpen();
    } else {
      this.animateClose();
    }

    let itemsClasses;

    if(this.props.section.products) {
      return(
        <ul className="nkTotem-Menu-Container">
          {
            this.props.section.products.map((item, i) => {
              itemsClasses = classNames('nkTotem-Menu-Container-Item--Products', {
                'is-Active': i == (this.props.slideUpdated || 0)
              });

              return (
                <li className={itemsClasses} key={i}>
                  { item[this.props.language].title }
                </li>
              );
            })
          }
        </ul>
      );
    }
  }

  render() {
    return (
      <nav className="nkTotem-Menu">
        { this.productsMenuTemplate() }
      </nav>
    )
  }
}
