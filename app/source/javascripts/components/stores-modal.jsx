import React, { Component } from 'react';
import classNames from 'classnames';
import { STATE } from '../helpers/constants';
import TweenMax from 'gsap';

import qrCodeHyperdunk from '../../../assets/images/qr-code/qr-focus.jpg';

export default class StoresModalComponent extends Component {
  constructor(props) {
    super(props);
  }

  componentDidUpdate() {
    if(this.props.open) {
      this.openModal();
    }

    if(!this.props.open) {
      this.closeModal();
    }
  }

  openModal(){
    TweenMax.to('.nkTotem-stores-modal', 1, { y: "0%", ease:"Power2.easeInOut", delay: 0.2 });
    TweenMax.to('.nkTotem-stores-modal-container',  1.1, { top: "50%", opacity : 1, ease: Back.easeOut.config(1.3), delay: 1.2 });
  }

  closeModal(){
    TweenMax.to('.nkTotem-stores-modal', 1, { y: "100%", ease:"Power2.easeInOut", delay: 0.7 });
    TweenMax.to('.nkTotem-stores-modal-container',  1.1, { top: "70%", opacity : 0, ease: Back.easeOut.config(1.3), delay: 0.5 });
  }

  renderStores() {
    let stores = this.props.section.products[this.props.product].stores;
    return (
      <div>
        {
          stores.map((item, i) => {
            return (
              <div className="nkTotem-stores-modal-list-item" key={i}>
                <h3 className="u-t-tradegothic-medium-2">{item}</h3>
              </div>
            )
          })
        }
      </div>
    )
  }


  render() {
    let qrCode = require(`../../../assets/images/qr-code/${this.props.section.products[this.props.product].qrcode}`);
    console.log(this.props.section.products[this.props.product].qrcode);
    return (
      <section className="nkTotem-stores-modal" onClick={() => { this.props.closeModal() }}>
        <div className="nkTotem-stores-modal-container">
          <div className="nkTotem-stores-modal-items">
            <div className="nkTotem-stores-modal-items-img">
              <img src={qrCode} />
              <p className="u-t-tradegothic-medium-2">{this.props.section.products[this.props.product].eCommerce}</p>
            </div>
          </div>

          <div className="nkTotem-stores-modal-items">
            <div className="nkTotem-stores-modal-list">

              <h2 className="u-t-tradegothic-medium">{`${ this.props.language == 'pt-br' ? 'Você também pode encontrar esse produto nas lojas:' : 'You can also find this product at our Nike stores:' }`}</h2>

              {this.props.section.products  ?  this.renderStores() : ''}

            </div>
          </div>
        </div>
      </section>
    )
  }
}
