import React, { Component } from 'react';
import PreHome from '../containers/pre-home';
import Home from '../containers/home';
import Internal from '../containers/internal';

export default class AppComponent extends Component {
  constructor(props) {
    super(props);
  }

  homeTemplate() {
    if( this.props.language ) {
      return <Home />;
    }
  }

  internalTemplate() {
    if( this.props.language ) {
      return <Internal />;
    }
  }

  render() {
    return (
      <div>
        <PreHome />
        { this.homeTemplate() }
        { this.internalTemplate() }
      </div>
    );
  }
}
