import React, { Component } from 'react';
import Hammer from 'hammerjs';
import bodymovin from 'bodymovin';
import classNames from 'classnames';
import Menu from '../containers/home/menu';
import Video from '../components/video';
import { STATE } from '../helpers/constants';

// import StoresModalComponent from '../components/stores-modal';

import videoHome from '../../../assets/videos/video-home.mp4';
import imageSwoosh from '../../../assets/images/swoosh.png';

export default class HomeComponent extends Component {
  constructor(props) {
    super(props);
  }

  get intervalHandler(){
    return this._intervalHandler;
  }

  set intervalHandler(value){
    this._intervalHandler = value;
  }

  componentDidMount() {
    this.initializeAutoPlay();
    this.animateOpen();
    this.animateAutoPlay();

    setTimeout(() => {
      this.initializeBodyMovin();
    }, 1400);
  }

  componentWillUpdate() {
    if( this.letterings ) {
      this.letterings.destroy();
    }

    if( this.props.resetInterval ) {
      this.initializeAutoPlay();
    }
  }

  componentDidUpdate() {
    if(this.letterings) {
      this.letterings.destroy();
      this.initializeBodyMovin();
      this.animateAutoPlay();
    }

    if( this.props.enter ) {
      this.initializeAutoPlay();
    }

    if( this.props.out ) {
      this.clearAutoPlay();
    }
  }

  animateAutoPlay() {
    if( this.refs.sectionImages.classList.contains('is-Changed') ) {
      this.refs.sectionImages.classList.remove('is-Changed');

      setTimeout(() => {
        this.refs.sectionImages.classList.add('is-Changed');
      }, 50);

    } else {
      this.refs.sectionImages.classList.add('is-Changed');
    }
  }

  animateOpen() {
    TweenMax.to('.nkTotem-Home', 1.2, { y: "0%", ease:"Power2.easeInOut", delay: 0.5 });
    TweenMax.to('.nkTotem-Home-Button', 0.7, { scale: 1, ease: Back.easeOut.config(1.6), delay: 1.6 });
  }

  clearAutoPlay() {
    clearInterval( this.intervalHandler );
  }

  initializeAutoPlay() {
    this.clearAutoPlay();

    this.intervalHandler = setInterval(() => {

      console.log('initializeAutoPlay');

      this.props.autoPlay();

    }, 5000);

  }

  initializeBodyMovin() {
    let getLetteringsAnimation = require(`../../../assets/bodymovin/home/${this.props.language}/${this.props.selected.letterings}`);

    this.letterings = bodymovin.loadAnimation({
      container: document.querySelector('.nkTotem-Home-Letterings'),
      renderer: 'svg',
      animationData: getLetteringsAnimation,
      autoPlay: true,
      loop: false
    });

    this.letterings.playSegments([[0, 18], [18, 20]], true);
  }

  initializeHammer() {
    let homeElement = document.querySelector('.nkTotem-Home');
    let homeHammer  = new Hammer(homeElement);

    homeHammer.get('swipe').set({
      direction: Hammer.DIRECTION_ALL
    });

    homeHammer.on("swipeleft", () => {
      this.props.swipeLeft();
      this.initializeAutoPlay();
    });

    homeHammer.on("swiperight", () => {
      this.props.swipeRight();
      this.initializeAutoPlay();
    });

    // homeHammer.on("swipeup", () => {
    //   this.props.openSectionDetail();
    //   if( this.props.enter ) {
    //     evt.srcEvent.preventDefault();
    //   }
    //   this.clearAutoPlay();
    // });
  }

  reloadPage() {
    window.location.reload();
  }

  prev(){
    this.props.swipeRight();
    this.initializeAutoPlay();
  }

  next(){
    this.props.swipeLeft();
    this.initializeAutoPlay();
  }

  homeTemplate() {
    if( this.props.selected ) {
      let sectionImage = require(`../../../assets/images/home/${this.props.selected.name}.jpg`);
      // <StoresModalComponent />

      return(
        <div>
          <div className={`nkTotem-Home-Video ${this.sectionChanged ? 'is-Changed' : ''}`} ref="sectionImages">
            <img src={sectionImage} />
          </div>

          <div className="nkTotem-Home-Swoosh">
            <img src={imageSwoosh} />
          </div>

          <div className="nkTotem-Home-Info">
            <div className="nkTotem-Home-Letterings">

            </div>
            <button className="nkTotem-Home-Button u-button" onClick={() => { this.props.openSectionDetail(); }}>{STATE.INITIAL.common['discover'][this.props.language]}</button>
          </div>

          <button type="button" data-role="none" className="slick-prev" onClick={ this.prev.bind( this ) }></button>
          <button type="button" data-role="none" className="slick-next" onClick={ this.next.bind( this ) } ></button>
        </div>
      );
    }
  }

  menuTemplate() {
    if( this.props.selected ) {
      return <Menu clear={ this.clearAutoPlay.bind(this) } />;
    }
  }

  promoterButton() {
    return <button className="u-Promoter-Button" onClick={this.reloadPage.bind(this)}></button>
  }

  render() {
    return (
      <div className="nkTotem-Home">
        { this.menuTemplate() }
        { this.homeTemplate() }
        { this.promoterButton() }
      </div>
    );
  }
}
