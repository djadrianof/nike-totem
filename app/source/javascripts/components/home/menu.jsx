import React, { PropTypes, Component } from 'react';
import classNames from 'classnames';

import flagPtBr from '../../../../assets/images/pt-br.png';
import flagEnUs from '../../../../assets/images/en-us.png';

export default class MenuComponent extends Component {
  constructor(props) {
    super(props);
  }

  toggleLocale(){
    this.props.language === 'pt-br' ? this.props.onSelectLanguage('en-us') : this.props.onSelectLanguage('pt-br')
  }

  render() {
    let itemsClasses;

    if( this.props.selected ) {
      return (
        <nav className="nkTotem-Menu">
          <ul className="nkTotem-Menu-Container">
            {
              this.props.sections.map((item, i) => {
                itemsClasses = classNames('nkTotem-Menu-Container-Item', {
                  'is-Active': item.name == this.props.selected.name
                });

                if( item.active ) {
                  return (
                    <li className={itemsClasses} key={i} data-section-name={item.name} onClick={(evt) => { this.props.onSelectMenu(evt) }}>
                      {item[this.props.language].name}
                    </li>
                  );
                }
              })
            }
          </ul>
        </nav>
      )
    }
  }
}
