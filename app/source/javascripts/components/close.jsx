import React, { Component } from 'react';
import classNames from 'classnames';
import TweenMax from 'gsap';

import closeIcon from '../../../assets/images/close.svg';

export default class CloseComponent extends Component {
  constructor(props) {
    super(props);
  }

  componentDidUpdate() {
    if( ~~this.props.doHide ) {
      TweenMax.to(this.refs[this.props.id], 1, { scale: 0, ease: Back.easeOut.config(1), delay: this.props.delayShow}, 0);
    } else {
      this.animateOpen();
    }
  }

  animateOpen() {
    TweenMax.to(this.refs[this.props.id], 0.5, { scale: 1, ease: Back.easeOut.config(1.5), delay: this.props.delayShow}, 0);
  }

  render() {
    return(
      <button className="u-Close-Button" ref={this.props.id} onClick={this.props.onClick}>
        <img src={closeIcon} />
      </button>
    );
  }
}
