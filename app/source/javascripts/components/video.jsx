import React, { Component } from 'react';
import classNames from 'classnames';

export default class VideoComponent extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    if( ~~this.props.autoplay ) {
      this.load();
      this.play();
    }
  }

  componentDidUpdate() {
    if( ~~this.props.pause ) {

      setTimeout(() => {
        this.pause();
      }, ~~this.props.delayPause );

    } else {
      this.load();
      this.play();
    }
  }

  load() {
    this.refs[this.props.id].load();
  }

  play() {
    this.refs[this.props.id].play();
  }

  pause() {
    this.refs[this.props.id].pause();
  }

  render() {
    return(
      <video muted loop ref={this.props.id} poster={this.props.frame}>
        <source src={this.props.video} type="video/mp4" />
      </video>
    );
  }
}
