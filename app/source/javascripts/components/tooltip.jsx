import React, { Component } from 'react';
import classNames from 'classnames';
import TweenMax from 'gsap';
import bodymovin from 'bodymovin';
import { STATE } from '../helpers/constants';

export default class TooltipComponent extends Component {
  constructor(props) {
    super(props);

    this.tooltipElements = {
      container: '.nkTotem-Tooltip-Circle'
    };
  }

  componentDidMount() {
    this.animateOpen();
  }

  animateOpen() {
    TweenMax.to(this.refs[this.props.id], 1.2, { scale: 1, ease : "Power2.easeInOut", delay: this.props.delayEnter, onComplete: () => { this.initializeBodyMovin(); } });
    TweenMax.to(this.refs[this.props.id], 1.2, { scale: 0, ease : "Power2.easeInOut", delay: this.props.delayOut, onComplete:(evt) => { this.letterings.destroy(); } });
  }

  initializeBodyMovin() {
    let getLetteringsAnimation = require(`../../../assets/bodymovin/tooltips/${this.props.language}/${this.props.id}.json`);

    this.letterings = bodymovin.loadAnimation({
      container: this.refs[this.props.id],
      renderer: 'svg',
      animationData: getLetteringsAnimation,
      autoPlay: true,
      loop: false
    });

  }

  render() {
    return(
      <div className="nkTotem-Tooltip">
        <div className="nkTotem-Tooltip-Circle" ref={this.props.id}></div>
      </div>
    );
  }
}
