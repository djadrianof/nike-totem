import React, { Component } from 'react';
import bodymovin from 'bodymovin';
import Video from '../components/video';
import { STATE } from '../helpers/constants';

import videoHome from '../../../assets/videos/video-home.mp4';
import imageSwoosh from '../../../assets/images/swoosh.png';
import imageBrazilFlag from '../../../assets/images/brazil.svg';
import imageUsFlag from '../../../assets/images/us.svg';

export default class PreHomeComponent extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.initializeBodyMovin();
  }

  initializeBodyMovin() {
    let getLetteringsAnimation = require(`../../../assets/bodymovin/home/pt-br/home.json`);

    this.letterings = bodymovin.loadAnimation({
      container: document.querySelector('.nkTotem-Pre-Home-Letterings'),
      renderer: 'svg',
      animationData: getLetteringsAnimation,
      autoPlay: true,
      loop: false
    });

    this.letterings.playSegments([[0, 18], [18, 20]], true);
  }

  selectLanguage(language, evt) {
    evt.preventDefault();
    this.props.onSelectLanguage(language);
  }

  render() {
    return (
      <div className="nkTotem-Pre-Home">
        <div className="nkTotem-Pre-Home-Video">
          <Video id="pre-home" video={videoHome} pause={`${ this.props.languageSelected ? 1 : 0 }`} autoplay="1" delayPause="1100" />
        </div>
        <div className="nkTotem-Pre-Home-Swoosh">
          <img src={imageSwoosh} />
        </div>
        <div className="nkTotem-Pre-Home-Info">
          <div className="nkTotem-Pre-Home-Letterings"></div>
        </div>
        <nav className="nkTotem-Pre-Home-Languages">
          <ul>
            <li onClick={this.selectLanguage.bind(this, 'en-us')}>
              <img src={imageUsFlag} />
            </li>
            <li onClick={this.selectLanguage.bind(this, 'pt-br')}>
              <img src={imageBrazilFlag} />
            </li>
          </ul>
        </nav>
      </div>
    );
  }
}
