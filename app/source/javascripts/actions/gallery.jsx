import { FUNCTIONS } from '../helpers/functions';
import { STATE } from '../helpers/constants';
import { homeActions } from '../actions/index';

export function loadedGalleryImages( images ) {
  return { type: 'LOADED_GALLERY_IMAGES', images }
}

export function load(open = false) {
  return (dispatch, getState) => {

    console.log("%c ------------------ GALLERY LOAD ------------------", "font-weight:bold;");

    let currentSection         = getState().menu.selected.item;
    let currentProductsSection = getState().products.slideUpdated.index;
    let currentSectionImages   = currentSection.gallery || currentSection.products[currentProductsSection || 0];

    dispatch({ type: 'LOADED_GALLERY_IMAGES', data: {
        gallery: currentSection.products[currentProductsSection || 0],
        section: currentSection.name
      }
    });

    if(open) {
      dispatch({ type: 'GALLERY_OPEN' });
    }

  };
}

export function close() {
  return (dispatch, getState) => {

    let currentSection         = getState().menu.selected.item;
    let currentProductsSection = getState().products.slideUpdated.index;

    if( currentSection.gallery ) {
      dispatch({ type: 'GALLERY_CLOSE' });
      dispatch(homeActions.enter());

      setTimeout(() => {
        dispatch({ type: 'GALLERY_DESTROY' });
      }, 1700);
    }

    if( currentSection.products ) {
      dispatch({ type: 'GALLERY_CLOSE' });
    }

  }
}

export function open() {
  return { type: 'GALLERY_OPEN' }
}

export function destroy() {
  return { type: 'GALLERY_DESTROY' }
}
