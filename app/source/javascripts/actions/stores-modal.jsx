export function open() {
  return { type: 'OPEN_STORES_MODAL' }
}

export function close() {
  return { type: 'CLOSE_STORES_MODAL' }
}
