export function open( section ) {
  return { type: 'TOOLTIP_OPEN', section }
}

export function close() {
  return { type: 'TOOLTIP_CLOSE' }
}
