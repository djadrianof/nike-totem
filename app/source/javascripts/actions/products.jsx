import { homeActions, galleryActions } from '../actions/index';

export function close() {
  return (dispatch, getState) => {

    dispatch({ type: 'PRODUCTS_CLOSE' });

    dispatch(galleryActions.destroy());
    dispatch(homeActions.enter());

    setTimeout(() => {
      dispatch({ type: 'PRODUCTS_RESET_SLIDE' });
    }, 1400);

  }
}

export function resetSlide() {
  return { type: 'PRODUCTS_RESET_SLIDE' }
}

export function slickInitialized() {
  return { type: 'PRODUCTS_SLICK_INITIALIZED' }
}


export function open() {
  return (dispatch, getState) => {

    dispatch({ type: 'PRODUCTS_SHOW' });
    dispatch(galleryActions.load());

  };
}

export function changeSlide( slide ) {
  return (dispatch, getState) => {

    dispatch({ type: 'PRODUCTS_CHANGE_SLIDE', slide });
    dispatch(galleryActions.load());

  };
}
