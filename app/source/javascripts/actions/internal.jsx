import { STATE } from '../helpers/constants';
import { FUNCTIONS } from '../helpers/functions';
import { homeActions, galleryActions, galleryDetailsActions, productsActions } from '../actions/index';

export function open() {
  return (dispatch, getState) => {

    let currentSection = getState().menu.selected.item;
    let getSectionData = FUNCTIONS.getInternalData( currentSection.name );

    // if( getSectionData.gallery ) {
    //   dispatch(galleryActions.load(true));
    //   dispatch(homeActions.out());
    // }

    // if( getSectionData.products ) {
    //   dispatch(productsActions.open());
    //   dispatch(homeActions.out());
    // }

    dispatch(productsActions.open());
    dispatch(homeActions.out());

  };
}
