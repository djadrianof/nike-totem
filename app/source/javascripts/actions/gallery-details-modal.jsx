export function changeDetail( image ) {
  return { type: 'CHANGE_GALLERY_DETAILS_MODAL', image }
}

export function open( image ) {
  return { type: 'OPEN_GALLERY_DETAILS_MODAL', image }
}

export function close() {
  return (dispatch, getState) => {
    dispatch({ type: 'CLOSE_GALLERY_DETAILS_MODAL' });

    setTimeout(() => {
      dispatch({ type: 'DESTROY_GALLERY_DETAILS_MODAL' });
    }, 1500);
  }
}
