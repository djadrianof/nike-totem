const getNextSectionMenu = (props) => {

  let currentSection = props.menuState.selected.item;
  let menuSections   = props.menuState.sections;
  let nextSectionMenu;

  for( let x = 0; x <= menuSections.length; x++ ) {
    if( currentSection.name == menuSections[x].name ) {
      if( menuSections[ props.orientation == 'left' ? (x + 1) : (x - 1) ] ) {
        nextSectionMenu = menuSections[ props.orientation == 'left' ? (x + 1) : (x - 1) ].name;
      } else {
        nextSectionMenu = menuSections[ props.orientation == 'left' ? 0 : (menuSections.length-1) ].name;
      }
      break;
    }
  }

  return nextSectionMenu;
}

export function swipeLeft() {
  return (dispatch, getState) => {

    console.log("%c ------------------ HOME SWIPE LEFT ------------------", "font-weight:bold;");

    let nextSectionMenu = getNextSectionMenu({
      menuState  : getState().menu,
      orientation: 'left'
    });

    dispatch({ type: 'SELECT_MENU', section: nextSectionMenu });

  };
}

export function swipeRight() {
  return (dispatch, getState) => {

    console.log("%c ------------------ HOME SWIPE RIGHT ------------------", "font-weight:bold;");

    let nextSectionMenu = getNextSectionMenu({
      menuState  : getState().menu,
      orientation: 'right'
    });

    dispatch({ type: 'SELECT_MENU', section: nextSectionMenu });

  };
}

export function autoPlay() {
  return (dispatch, getState) => {

    console.log("%c ------------------ HOME AUTOPLAY ------------------", "font-weight:bold;");

    let nextSectionMenu = getNextSectionMenu({
      menuState  : getState().menu,
      orientation: 'left'
    });

    dispatch({ type: 'SELECT_MENU', section: nextSectionMenu });

  };
}

export function open() {
  return (dispatch, getState) => {

    console.log("%c ------------------ HOME OPEN ------------------", "font-weight:bold;");

    dispatch({ type: 'SELECT_MENU', section: 'running' });
    dispatch({ type: 'OPEN_HOME' });

  };
}

export function enter() {
  return { type: 'ENTER_ON_HOME' }
}

export function out() {
  return { type: 'OUT_OF_HOME' }
}

export function clearInterval() {
  return { type: 'CLEAR_INTERVAL_HOME' }
}
