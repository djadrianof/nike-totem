import { menuActions } from '../actions/index';

export function start( language ) {
  return { type: 'START_APP', language }
}

export function language( language ) {
  return (dispatch, getState) => {

  dispatch({ type: 'SELECT_LANGUAGE', language });
  dispatch(menuActions.select('running'));

  }
}
