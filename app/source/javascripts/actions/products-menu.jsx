export function selectProductsMenu( product ) {
  return { type: 'SELECT_PRODUCTS_MENU', product }
}
