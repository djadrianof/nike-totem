const galleryDetailsState = {
  loaded: false,
  images: null,
  open: null
};

const galleryDetailsReducer = (state = galleryDetailsState, action) => {

  let newState = Object.assign({}, state);

  switch(action.type) {
    case 'LOADED_GALLERY_DETAILS_IMAGES':
      newState.loaded  = true;
      newState.images  = action.data.gallery;
      newState.section = action.data.section;
      return newState;

    default:
      return state;
  }
};

export default galleryDetailsReducer;
