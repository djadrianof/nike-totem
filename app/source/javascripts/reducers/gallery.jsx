const galleryState = {
  loaded: false,
  open: false,
  data: null,
  section: null
};

const galleryReducer = (state = galleryState, action) => {
  let newState = Object.assign({}, state);

  switch(action.type) {
    case 'LOADED_GALLERY_IMAGES':
      newState.loaded  = true;
      newState.data    = action.data.gallery;
      newState.section = action.data.section;
      return newState;

    case 'GALLERY_OPEN':
      newState.open = true;
      return newState;

    case 'GALLERY_CLOSE':
      newState.open = false;
      return newState;

    case 'GALLERY_DESTROY':
      newState.data = null;
      newState.section = null;
      return newState;

    default:
      return state;
  }
};

export default galleryReducer;
