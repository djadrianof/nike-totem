import { FUNCTIONS } from '../helpers/functions';
import { STATE } from '../helpers/constants';

const menuState = {
  sections: STATE.INITIAL.sections,
  selected: {
    item: null
  }
};

const menuReducer = (state = menuState, action) => {
  let newState = Object.assign({}, state);

  switch(action.type) {
    case 'SELECT_MENU':
      newState.selected.item = FUNCTIONS.getInternalData( action.section );
      return newState;

    default:
      return state;
  }
};

export default menuReducer;
