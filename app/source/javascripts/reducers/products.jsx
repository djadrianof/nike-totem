const productsState = {
  open        : false,
  slideUpdated: {
    index: 0
  },
  slick: false
};

const menuReducer = (state = productsState, action) => {
  let newState = Object.assign({}, state);

  switch(action.type) {
    case 'PRODUCTS_CHANGE_SLIDE':
      newState.slideUpdated.index = action.slide;
      return newState;

    case 'PRODUCTS_SHOW':
      newState.open = true;
      return newState;

    case 'PRODUCTS_CLOSE':
      newState.open = false;
      return newState;

    case 'PRODUCTS_SLICK_INITIALIZED':
      newState.slick = true;
      return newState;

    case 'PRODUCTS_RESET_SLIDE':
      newState.slideUpdated.index = 0;
      return newState;

    default:
      return state;
  }
};

export default menuReducer;
