import { STATE } from '../helpers/constants';

const storesModalState = {
  open: false
};

const storesModalReducer = (state = storesModalState, action) => {
  let newState = Object.assign({}, state);

  switch(action.type) {
    case 'OPEN_STORES_MODAL':
      newState.open = true;
      return newState;

    case 'CLOSE_STORES_MODAL':
      newState.open = false;
      return newState;

    default:
      return state;
  }
};

export default storesModalReducer;
