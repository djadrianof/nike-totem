import { FUNCTIONS } from '../helpers/functions';

const appState = {
  initialized: null,
  language   : null,
  tooltip    : true
};

const appReducer = (state = appState, action) => {
  let newState = Object.assign({}, state);

  switch(action.type) {
    case 'START_APP':
      newState.initialized = true;
      return newState;

    case 'SELECT_LANGUAGE':
      newState.language = action.language;
      return newState;

    case 'OPEN_PRODUCTS':
      newState.detail = action.section;
      return newState;

    default:
      return state;
  }
};

export default appReducer;
