import { FUNCTIONS } from '../helpers/functions';

const homeState = {
  enter: false,
  out: false,
  opened: false,
  resetInterval: true
};

const homeReducer = (state = homeState, action) => {
  let newState = Object.assign({}, state);

  switch(action.type) {
    case 'ENTER_ON_HOME':
      newState.enter = true;
      newState.out = false;
      return newState;

    case 'OUT_OF_HOME':
      newState.enter = false;
      newState.out = true;
      return newState;

    case 'OPEN_HOME':
      newState.opened = true;
      return newState;

    case 'CLEAR_INTERVAL_HOME':
      newState.resetInterval = true;
      return newState;

    default:
      return state;
  }
};

export default homeReducer;
