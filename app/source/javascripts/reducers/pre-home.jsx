import { FUNCTIONS } from '../helpers/functions';

const preHomeState = {
  enter: false,
  out: false
};

const preHomeReducer = (state = preHomeState, action) => {
  let newState = Object.assign({}, state);

  switch(action.type) {
    case 'ENTER_ON_HOME':
      newState.enter = true;
      newState.out = false;
      return newState;

    case 'OUT_OF_HOME':
      newState.enter = false;
      newState.out = true;
      return newState;

    default:
      return state;
  }
};

export default preHomeReducer;
