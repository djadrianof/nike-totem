import { STATE } from '../helpers/constants';

const productsMenuState = {
  selected: null
};

const productsMenuReducer = (state = productsMenuState, action) => {
  let newState = Object.assign({}, state);

  switch(action.type) {
    case 'SELECT_PRODUCTS_MENU':
      newState.selected = action.product

      return newState;
    default:
      return state;
  }
};

export default productsMenuReducer;
