const galleryDetailsModalState = {
  open: null,
  detail: null
};

const galleryDetailsModalReducer = (state = galleryDetailsModalState, action) => {

  let newState = Object.assign({}, state);

  switch(action.type) {
    case 'OPEN_GALLERY_DETAILS_MODAL':
      newState.open = true;
      newState.detail = 0;
      return newState;

    case 'CLOSE_GALLERY_DETAILS_MODAL':
      newState.open = false;
      return newState;

    case 'CHANGE_GALLERY_DETAILS_MODAL':
      newState.detail = action.image;
      return newState;

    case 'DESTROY_GALLERY_DETAILS_MODAL':
      newState.detail = null;
      return newState;

    default:
      return state;
  }
};

export default galleryDetailsModalReducer;
