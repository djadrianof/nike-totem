import { combineReducers } from 'redux';

// -----------------------------------------------------
// Load Reducers
// -----------------------------------------------------

import menuReducer from './menu';
import appReducer from './app';
import galleryReducer from './gallery';
import galleryDetailsReducer from './gallery-details';
import productsReducer from './products';
import productsMenuReducer from './products-menu';
import tooltipReducer from './tooltip';
import homeReducer from './home';
import galleryDetailsModalReducer from './gallery-details-modal';
import storesModalReducer from './stores-modal';

// -----------------------------------------------------
// Share reducers to application
// -----------------------------------------------------

const reducers = combineReducers({
  menu          : menuReducer,
  app           : appReducer,
  gallery       : galleryReducer,
  galleryDetails: galleryDetailsReducer,
  products      : productsReducer,
  productsMenu  : productsMenuReducer,
  tooltip       : tooltipReducer,
  home          : homeReducer,
  galleryDetailsModal : galleryDetailsModalReducer,
  storesModal : storesModalReducer
});

export default reducers;
