import { FUNCTIONS } from '../helpers/functions';

const tooltipState = {
  opened: false,
  data: null
};

const tooltipReducer = (state = tooltipState, action) => {
  let newState = Object.assign({}, state);

  switch(action.type) {
    case 'TOOLTIP_OPEN':
      newState.opened = true;
      newState.data   = FUNCTIONS.getTooltip( action.section );
      return newState;

    case 'TOOLTIP_CLOSE':
      newState.opened = false;
      return newState;

    default:
      return state;
  }
};

export default tooltipReducer;
