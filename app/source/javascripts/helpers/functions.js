import Q from 'q';
import { STATE } from '../helpers/constants';

class FUNCTIONS {
  static preloadImages( images = [], path, isDetail = false ) {

    let arrPromises = [];

    if( images.length ) {
      images.map((item, i) => {

        let deferred = Q.defer();
        let imgCache = new Image();
        let imageName = isDetail ? item.image : item;

        let imageLoad = require(`../../../../app/assets/images/${path}/${imageName}`);

        imgCache.onload = () => {
          deferred.resolve();
        }

        imgCache.src = imageLoad;
        arrPromises.push( deferred.promise );

      });

      return Q.all( arrPromises );
    }

  }

  static getInternalData( section ) {
    let apiSections = STATE.INITIAL.sections.filter((item, i) => {
      return item.name === section;
    });

    return apiSections[ 0 ];
  }

  static getTooltip( section ) {
    return STATE.INITIAL.tooltips[ section ];
  }
}

export { FUNCTIONS };
