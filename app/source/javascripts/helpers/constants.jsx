class STATE {
  static get INITIAL () {
    return {
      "sections": [
        {
          "name": "running",
          "pt-br": {
            "name": "Corrida",
            "description": "Lamaaaina 1 sae nas baldas foi sahido altura. Paixao bolota que com cruzes comigo amo craves bolsos bem"
          },
          "en-us": {
            "name": "running",
            "description": "Lamina 2 sae nas baldas foi sahido altura. Paixao bolota que com cruzes comigo amo craves bolsos bem"
          },
          "products": [
            {
              "pt-br": {
                "title": "LUNAREPIC FLYKNIT",
                "description": "DISTÂNCIA SEM LIMITES"
              },
              "en-us": {
                "title": "LUNAREPIC FLYKNIT",
                "description": "UNLIMITED DISTANCE"
              },
              "image": '01-lunar.jpg',
              "images": [
                '01-lunar.jpg',
                '02-lunar.jpg',
                '03-lunar.jpg',
                '04-lunar.jpg'
              ],
              "details": [
                {
                  "thumb": '01-lunar-thumb.jpg',
                  "image": '01-lunar-gallery.jpg',
                  "pt-br": {
                    "title": ' LUNAREPIC FLYKNIT',
                    "description": ' Corra para sempre com conforto absoluto para uma corrida revolucionária.'
                  },
                  "en-us": {
                    "title": ' LUNAREPIC FLYKNIT',
                    "description": 'Absolute comfort, effortless ride.'
                  }
                },
                {
                  "thumb": '02-lunar-thumb.jpg',
                  "image": '02-lunar.jpg',
                  "pt-br": {
                    "title": 'AJUSTE CONFORTÁVEL',
                    "description": 'O cabedal em Flyknit com cano médio é praticamente imperceptível, melhorando a sensação da sua pisada.'
                  },
                  "en-us": {
                    "title": 'VANISHING FIT',
                    "description": 'Mid-rise Flyknit upper disappears for fantastic sensation underfoot.'
                  }
                },
                {
                  "thumb": '03-lunar-thumb.jpg',
                  "image": '03-lunar.jpg',
                  "pt-br": {
                    "title": 'SENSAÇÃO DE LEVEZA',
                    "description": 'A entressola é feita com espuma Lunarlon moldada com cortes a laser, trazendo suavidade para a sua corrida.'
                  },
                  "en-us": {
                    "title": 'fluid feel',
                    "description": 'Precision-lasered Lunarlon midsole delivers a perfectly smooth ride.'
                  }
                },
                {
                  "thumb": '04-lunar-thumb.jpg',
                  "image": '04-lunar.jpg',
                  "pt-br": {
                    "title": 'INCRIVELMENTE MACIO',
                    "description": 'O solado Lunarlon, com mapeamento de pressão da passada, proporciona um amortecimento preciso.'
                  },
                  "en-us": {
                    "title": 'Superb softness',
                    "description": 'Pressure-mapped outsole provides targeted cushioning.'
                  }
                }
              ],
              "stores": [
                'Nike Ipanema',
                'Nike Copacabana',
                'Nike  RioSul shopping',
                'Nike Barra Shopping',
                'Nike Niteroi Plaza',
                'Nike Leblon',
              ],
              "eCommerce" : "gonike.me/comprarLunarEpic",
              "qrcode": "qr-lunarepic.jpg",
              "video": "flyknit.webm",
              "video-frame": 'lunar.jpg'
            },
            {
              "pt-br": {
                "title": "LUNAREPIC FLYKNIT LOW",
                "description": "DISTÂNCIA SEM LIMITES"
              },
              "en-us": {
                "title": "LUNAREPIC FLYKNIT LOW",
                "description": "UNLIMITED DISTANCE"
              },
              "image": '01-lunar-low.jpg',
              "images": [
                '01-lunar-low.jpg',
                '02-lunar-low.jpg',
                '03-lunar-low.jpg',
                '04-lunar-low.jpg'
              ],
              "details": [
                {
                  "thumb": '01-lunar-low-thumb.jpg',
                  "image": '01-lunar-low-gallery.jpg',
                  "pt-br": {
                    "title": ' LUNAREPIC FLYKNIT LOW',
                    "description": 'Corra para sempre com conforto absoluto para uma corrida revolucionária.'
                  },
                  "en-us": {
                    "title": ' LUNAREPIC FLYKNIT LOW',
                    "description": 'Breathable comfort, effortless ride. '
                  }
                },
                {
                  "thumb": '02-lunar-low-thumb.jpg',
                  "image": '02-lunar-low.jpg',
                  "pt-br": {
                    "title": 'AJUSTE CONFORTÁVEL',
                    "description": 'O cabedal em Flyknit com cano baixo melhora a sensação da sua pisada.'
                  },
                  "en-us": {
                    "title": 'ULTRA-LIGHT SUPPORT',
                    "description": 'Flyknit upper for ultra-lightweight support and breathability. '
                  }
                },
                {
                  "thumb": '03-lunar-low-thumb.jpg',
                  "image": '03-lunar-low.jpg',
                  "pt-br": {
                    "title": 'INCRIVELMENTE MACIO',
                    "description": 'O solado Lunarlon, com mapeamento de pressão da passada, proporciona um amortecimento preciso.'
                  },
                  "en-us": {
                    "title": 'fluid feel',
                    "description": 'Precision-lasered Lunarlon midsole delivers a smooth ride.'
                  }
                },
                {
                  "thumb": '04-lunar-low-thumb.jpg',
                  "image": '04-lunar-low.jpg',
                  "pt-br": {


                    "title": 'SENSAÇÃO DE LEVEZA',
                    "description": 'A entressola é feita com espuma Lunarlon moldada com cortes a laser, trazendo suavidade para a sua corrida.'
                  },
                  "en-us": {
                    "title": 'Superb softness',
                    "description": 'Pressure-mapped outsole provides targeted cushioning.'
                  }
                }
              ],
              "stores": [
                'Nike Ipanema',
                'Nike Copacabana',
                'Nike  RioSul shopping',
                'Nike Barra Shopping',
                'Nike Niteroi Plaza',
                'Nike Leblon',
              ],
              "eCommerce" : "gonike.me/comprarLunarEpicLow",
              "qrcode": "qr-lunarepiclow.jpg",
              "video": "lunarepic.webm",
              "video-frame": 'lunar-low.jpg'
            },
          ],
          "gallery": null,
          "active": true,
          "letterings": "running.json"
        },
        {
          "name": "training",
          "pt-br": {
            "name": "treino",
            "description": ""
          },
          "en-us": {
            "name": " TRAINING",
            "description": ""
          },
          "products": [
            {
              "pt-br": {
                "title": "Free Tr UltraFast Flyknit",
                "description": "VERSATILIDADE SEM LIMITES"
              },
              "en-us": {
                "title": "Free Tr UltraFast Flyknit",
                "description": "UNLIMITED VERSATILITY"
              },
              "image": '01-ultrafast.jpg',
              "images": [
                '02-ultrafast.jpg',
                '03-ultrafast.jpg',
                '04-ultrafast.jpg'
              ],
              "details": [
                {
                  "thumb": '01-ultrafast-thumb.jpg',
                  "image": '01-ultrafast-gallery.jpg',
                  "pt-br": {
                    "title": ' Free Train UltraFast Flyknit',
                    "description": 'Estabilidade dinâmica em qualquer tipo de treino, com a resposta e a firmeza que você precisa para ir cada vez mais rápido.'
                  },
                  "en-us": {
                    "title": ' Free Train UltraFast Flyknit',
                    "description": 'Train ultra fast with a foot-containing Chainlink Flyknit upper and ultra-responsive midsole propulsion plate.'
                  }
                },
                {
                  "thumb": '04-ultrafast-thumb.jpg',
                  "image": '04-ultrafast.jpg',
                  "pt-br": {
                    "title": 'Força',
                    "description": 'Entressola em perfil baixo e placa de impulsão interna oferecem máxima resposta.  '
                  },
                  "en-us": {
                    "title": 'RESPONSIVE FEEL',
                    "description": 'Low-profile midsole with responsive propulsion plate underfoot'
                  }
                },
                {
                  "thumb": '03-ultrafast-thumb.jpg',
                  "image": '03-ultrafast.jpg',
                  "pt-br": {
                    "title": 'Movimento',
                    "description": 'Cabedal em Flyknit com tramas na região do médio pé para máximo suporte.'
                  },
                  "en-us": {
                    "title": 'LOCKED-DOWN FIT',
                    "description": 'Nike Chainlink Flyknit upper offers ultra-responsive containment. '
                  }
                },
                {
                  "thumb": '02-ultrafast-thumb.jpg',
                  "image": '02-ultrafast.jpg',
                  "pt-br": {
                    "title": 'Tração',
                    "description": 'Solado desenvolvido para estabilidade e tração em diferentes superficies. '
                  },
                  "en-us": {
                    "title": 'STABILITY AND GRIP',
                    "description": 'Outsole designed for stability and traction on multiple surfaces.  '
                  }
                }
              ],
              "stores": [
                'NIKE IPANEMA',
                'NIKE LEBLON',
                'NIKE BARRA SHOPPING',
                'NIKE RIOSUL SHOPPING CENTER',
                'AUTHENTIC FEET BARRA SHOPPING',
                'AUTHENTIC FEET RIOSUL SHOPPING CENTER'
              ],
              "eCommerce" : "gonike.me/comprarUltrafast",
              "qrcode": "qr-ultrafast.jpg",
              "video": "ultrafast.webm",
              "video-frame": 'ultrafast.jpg'
            },
            {
              "pt-br": {
                "title": "Free Tr Focus Flyknit",
                "description": "EQUILÍBRIO SEM LIMITES"
              },
              "en-us": {
                "title": "Free Tr Focus Flyknit",
                "description": "UNLIMITED BALANCE"
              },
              "image": '01-focus.jpg',
              "images": [
                '01-focus.jpg',
                '02-focus.jpg',
                '03-focus.jpg',
                '04-focus.jpg'
              ],
              "details": [
                {
                  "thumb": '01-focus-thumb.jpg',
                  "image": '01-focus-gallery.jpg',
                  "pt-br": {
                    "title": ' Free Tr Focus Flyknit',
                    "description": 'Fixação dinâmica e flexibilidade respirável para você se movimentar com rapidez.'
                  },
                  "en-us": {
                    "title": ' Free Tr Focus Flyknit',
                    "description": 'Lightweight support combines with great flexibility to keep you moving quickly.'
                  }
                },
                {
                  "thumb": '04-focus-thumb.jpg',
                  "image": '04-focus.jpg',
                  "pt-br": {
                    "title": 'AJUSTE DINÂMICO',
                    "description": 'Cabos Flywire no antepé proporcionam um ajuste dinâmico.'
                  },
                  "en-us": {
                    "title": 'DYNAMIC LOCKDOWN',
                    "description": 'Forefoot Flywire cables deliver dynamic lockdown.'
                  }
                },
                {
                  "thumb": '02-focus-thumb.jpg',
                  "image": '02-focus.jpg',
                  "pt-br": {
                    "title": 'FLEXIBILIDADE RESPIRÁVEL',
                    "description": 'Cabedal em Flyknit oferece flexibilidade respirável.'
                  },
                  "en-us": {
                    "title": 'FLEXIBLE SUPPORT',
                    "description": 'Breatheable Flyknit hugs your foot for flexible support. '
                  }
                },
                {
                  "thumb": '03-focus-thumb.jpg',
                  "image": '03-focus.jpg',
                  "pt-br": {
                    "title": 'FLEXIBILIDADE E ESTABILIDADE',
                    "description": 'O padrão com detalhes de três pontas no solado se expande e se contrai a cada passo para conforto natural.'
                  },
                  "en-us": {
                    "title": 'FLEXIBILITY AND STABILTY',
                    "description": 'Tri-star outsole patter delivers flexibility and stability.'
                  }
                }
              ],
              "stores": [
                'Nike Ipanema',
                'Nike Barra Shopping',
                'Nike  RioSul shopping',
                'Nike Leblon',
                'Paquetá Barra Shopping'
              ],
              "eCommerce" : "gonike.me/comprarFreeFocus",
              "qrcode": "qr-focus.jpg",
              "video": "focus.webm",
              "video-frame": 'focus.jpg'
            }
          ],
          "gallery": null,
          "active": true,
          "letterings": "training.json"
        },
        {
          "name": "football",
          "pt-br": {
            "name": "futebol",
            "description": "Lamina sae nas baldas foi sahido altura. Paixao bolota que com cruzes comigo amo craves bolsos bem"
          },
          "en-us": {
            "name": "football",
            "description": "Lamina sae nas baldas foi sahido altura. Paixao bolota que com cruzes comigo amo craves bolsos bem"
          },
          "products": [
            {
              "pt-br": {
                "title": "MAGISTA OBRA II",
                "description": "CRIAÇÃO SEM LIMITES"
              },
              "en-us": {
                "title": "MAGISTA OBRA II",
                "description": "UNLIMITED PLAYMAKING"
              },
              "images": [
                '01-magista.jpg',
                '02-magista.jpg'
              ],
              "details": [
                {
                  "thumb": '01-magista-thumb.jpg',
                  "image": '01-magista-gallery.jpg',
                  "pt-br": {
                    "title": " MAGISTA OBRA II",
                    "description": 'Jogue rápido. Pense Rápido. Para controle total da bola, as chuteiras Nike Magista foram desenvolvidas para a criação de jogadas, combinando uma textura 3D revolucionária, um ajuste preciso e travas cônicas que oferecem tração 360 graus.'
                  },
                  "en-us": {
                    "title": " MAGISTA OBRA II",
                    "description": 'Play fast. Think fast. For total ball control, Nike Magista boots are made for creative playmaking, combiningrevolutionary 3-D texture, a lockdown fit and a 360° rotational traction pattern.'
                  }
                },
                {
                  "thumb": '02-magista-thumb.jpg',
                  "image": '02-magista.jpg',
                  "pt-br": {
                    "title": 'TRAÇÃO',
                    "description": 'As placas criadas com base em dados científicos aumentam a tração para movimentos de rotação.'
                  },
                  "en-us": {
                    "title": 'TRACTION',
                    "description": 'Data informed plate improves traction for rotational movement. '
                  }
                },
                {
                  "thumb": '03-magista-thumb.jpg',
                  "image": '03-magista.jpg',
                  "pt-br": {
                    "title": 'AJUSTE',
                    "description": 'A parte superior em Flyknit e o sistema de controle Nike Grip proporcionam ajuste firme.'
                  },
                  "en-us": {
                    "title": 'FIT',
                    "description": 'Flyknit upper and Nike Grip system provide lockdown fit.'
                  }
                },
                {
                  "thumb": '04-magista-thumb.jpg',
                  "image": '04-magista.jpg',
                  "pt-br": {
                    "title": 'TOQUE',
                    "description": 'Flyknit com sistema de fabricação térmica traz maior sensibilidade no toque de bola em todas as condições.'
                  },
                  "en-us": {
                    "title": 'TOUCH',
                    "description": 'Thermformed Flyknit for enhanced foot-to-ball feel in all conditions. '
                  }
                }
              ],
              "stores": [
                'Nike Copacabana',
                'Nike  RioSul shopping',
                'Paquetá Barra Shopping'
              ],
              "eCommerce" : "gonike.me/comprarMagista",
              "qrcode": "qr-magista.jpg",
              "video": 'magista.webm',
              "video-frame": 'magista.jpg'
            }
          ],
          "gallery": null,
          "active": true,
          "letterings": "football.json"
        },
        {
          "name": "basketball",
          "pt-br": {
            "name": "Basquete",
            "description": ""
          },
          "en-us": {
            "name": "basketball",
            "description": ""
          },
          "products": [
            {
              "pt-br": {
                "title": "Hyperdunk 2016 Flyknit",
                "description": "IMPULSÃO SEM LIMITES"
              },
              "en-us": {
                "title": "Hyperdunk 2016 Flyknit",
                "description": "Unlimited Elevation"
              },
              "image": '01-hyperdunk.jpg',
              "images": [
                '01-hyperdunk.jpg',
                '02-hyperdunk.jpg',
                '03-hyperdunk.jpg',
                '04-hyperdunk.jpg'
              ],
              "details": [
                {
                  "thumb": '01-hyperdunk-thumb.jpg',
                  "image": '01-hyperdunk-gallery.jpg',
                  "pt-br": {
                    "title": ' HYPERDUNK 2016 FLYKNIT',
                    "description": 'O suporte de um cano alto em um design versátil que acompanha o movimento natural do seu pé.'
                  },
                  "en-us": {
                    "title": 'HYPERDUNK 2016 FLYKNIT',
                    "description": 'High-top support in a versatile, low-top design that moves effortlessly with your foot. '
                  }
                },
                {
                  "thumb": '02-hyperdunk-thumb.jpg',
                  "image": '02-hyperdunk.jpg',
                  "pt-br": {
                    "title": 'SUPORTE FLEXÍVEL',
                    "description": 'O forro de compressão em malha Mesh oferece suporte, conforto e movimento ao tornozelo.'
                  },
                  "en-us": {
                    "title": 'FLEXIBLE SUPPORT',
                    "description": 'Compression sleeve enhances support and ankle flexibility.'
                  }
                },
                {
                  "thumb": '03-hyperdunk-thumb.jpg',
                  "image": '03-hyperdunk.jpg',
                  "pt-br": {
                    "title": 'AMORTECIMENTO RESPONSIVO',
                    "description": 'A unidade Nike Zoom Air integral de perfil baixo oferece excelente agilidade nas respostas.'
                  },
                  "en-us": {
                    "title": 'RESPONSIVE CUSHONING',
                    "description": 'Full-length Nike Zoom Air-Sole unit delivers responsive cushioning.'
                  }
                },
                {
                  "thumb": '04-hyperdunk-thumb.jpg',
                  "image": '04-hyperdunk.jpg',
                  "pt-br": {
                    "title": 'AJUSTE DINÂMICO',
                    "description": 'O cabedal em Flyknit oferece encaixe perfeito, ventilação e flexibilidade natural, enquanto os cabos Flywire dão estabilidade à parte intermediária do pé.'
                  },
                  "en-us": {
                    "title": 'DYNAMIC FIT',
                    "description": 'Flyknit and Flywire technology combine for a dynamic fit.'
                  }
                }
              ],
              "stores": [
                'Nike Copacabana',
                'Nike Norte Shopping',
                'Artwalk RioSul shopping',
                'Authentic Feet Riosul shopping',
                'Authentic Feet Barra Shopping'
              ],
              "eCommerce" : "gonike.me/comprarHyperdunk",
              "video": 'hyperdunk.webm',
              "qrcode": "qr-hyperdunk.jpg",
              "video-frame": 'hyperdunk.jpg'
            },
            {
              "pt-br": {
                "title": "Zoom KD9 Flyknit",
                "description": "MOVIMENTOS SEM LIMITES"
              },
              "en-us": {
                "title": "Zoom KD9 Flyknit",
                "description": "Unlimited Moves"
              },
              "image": '01-ZoomKD.jpg',
              "images": [
                '01-ZoomKD.jpg',
                '02-ZoomKD.jpg'
              ],
              "details": [
                {
                  "thumb": '01-ZoomKD-thumb.jpg',
                  "image": '01-ZoomKD-gallery.jpg',
                  "pt-br": {
                    "title": 'ZOOM KD9 FLYKNIT',
                    "description": 'Resposta rápida por todos os ângulos. Tecnologia Zoom por toda a extensão para criação de jogadas.'
                  },
                  "en-us": {
                    "title": 'ZOOM KD9 FLYKNIT',
                    "description": 'Responsive at every angle. Enhanced responsiveness and control for versatile play.'
                  }
                },
                {
                  "thumb": '02-ZoomKD-thumb.jpg',
                  "image": '02-ZoomKD.jpg',
                  "pt-br": {
                    "title": 'Caimento Perfeito',
                    "description": 'Tecnologia Flyknit presente na parte superior oferece respirabilidade,  elasticidade e suporte.'
                  },
                  "en-us": {
                    "title": 'INCREDIBLE FIT',
                    "description": 'Flyknit forefoot offers breathability, stretch and support.'
                  }
                },
                {
                  "thumb": '03-ZoomKD-thumb.jpg',
                  "image": '03-ZoomKD.jpg',
                  "pt-br": {
                    "title": 'Suporte com Fixação',
                    "description": 'Suporte que proporciona maior estabilidade.'
                  },
                  "en-us": {
                    "title": 'LOCKED-DOWN SUPPORT',
                    "description": 'Midfoot cage locks down yout foot.'
                  }
                },
                {
                  "thumb": '04-ZoomKD-thumb.jpg',
                  "image": '04-ZoomKD.jpg',
                  "pt-br": {
                    "title": 'Máxima Resposta',
                    "description": 'Amortecimento Nike Zoom Air por toda a extensão do tênis para resposta rápida.'
                  },
                  "en-us": {
                    "title": 'EXTREME RESPONSIVENESS',
                    "description": 'Full-length Nike Zoon Air-Sole unit delivers responsive cushioning. '
                  }
                }
              ],
              "stores": [
                'Nike Copacabana',
                'Nike Norte Shopping',
                'Artwalk RioSul shopping',
                'Authentic Feet Riosul shopping',
                'Authentic Feet Barra Shopping'
              ],
              "eCommerce" : "gonike.me/comprarKD9",
              "video": 'zoom.webm',
              "qrcode": "qr-kd9.jpg",
              "video-frame": 'zoom.jpg'
            }
          ],
          "gallery": null,
          "active": true,
          "letterings": "basketball.json"
        },
        {
          "name": "sportswear",
          "pt-br": {
            "name": "casual",
            "description": "Lamina sae nas baldas foi sahido altura. Paixao bolota que com cruzes comigo amo craves bolsos bem"
          },
          "en-us": {
            "name": "sportswear",
            "description": "Lamina sae nas baldas foi sahido altura. Paixao bolota que com cruzes comigo amo craves bolsos bem"
          },
          "products": [
            {
              "pt-br": {
                "title": "AIR MAX 1 ULTRA FLYKNIT",
                "description": "AIR SEM LIMITES"
              },
              "en-us": {
                "title": "AIR MAX 1 ULTRA FLYKNIT",
                "description": "Unlimited Air"
              },
              "images": [
                '01-airmax.jpg',
                '02-airmax.jpg'
              ],
              "details": [
                {
                  "thumb": '01-airmax-thumb.jpg',
                  "image": '01-airmax-gallery.jpg',
                  "pt-br": {
                    "title": 'AIR MAX 1 ULTRA FLYKNIT',
                    "description": 'O AM1 mais leve. A versão mais leve do ícone revolucionário com cabedal em Nike Flyknit, entressola Ultra e amortecimento Max Air para mais conforto o dia todo.'
                  },
                  "en-us": {
                    "title": 'AIR MAX 1 ULTRA FLYKNIT',
                    "description": 'The lightest 1. The lightest version of the revolutionary icon, remixed for the first time with a Flyknit  upper, an Ultra midsole and plush Air Max cushioning for all-day comfort.'
                  }
                },
                {
                  "thumb": '03-airmax-thumb.jpg',
                  "image": '03-airmax.jpg',
                  "pt-br": {
                    "title": 'AIR',
                    "description": 'Leve como o ar. O famoso amortecimento de ar visível - graças à unidade Max Air no calcanhar - proporciona amortecimento super leve para uma pisada incomparável.'
                  },
                  "en-us": {
                    "title": 'AIR',
                    "description": 'Light as air. Legendary visible-air cushioning – courtesy of a Max Air heel unit – delivers lightweight cushioning for an unmatched ride. '
                  }
                },
                {
                  "thumb": '02-airmax-thumb.jpg',
                  "image": '02-airmax.jpg',
                  "pt-br": {
                    "title": 'ULTRA',
                    "description": 'Viaje ultra leve. A entressola com espuma e o solado minimalista e reduzido proporcionam leveza e flexibilidade superiores.'
                  },
                  "en-us": {
                    "title": 'ULTRA',
                    "description": 'Travel ultra light. A foam Ultra midsole and a minimalist, slimmed down outsole bring the ultimate in lightweight flexibility.'
                  }
                },
                {
                  "thumb": '04-airmax-thumb.jpg',
                  "image": '04-airmax.jpg',
                  "pt-br": {
                    "title": 'FLYKNIT',
                    "description": 'O legado encontra a inovação. O cabedal em Flyknit leve e ventilável é feito com fibras altamente resistentes e costura minuciosa em áreas onde você mais precisa para oferecer elasticidade, suporte e conforto para os seus pés.'
                  },
                  "en-us": {
                    "title": 'FLYKNIT',
                    "description": 'Heritage meets innovation. A light, breathable Flyknit upper is made with high-strength fibers and precise stitching in targeted areas for stretch, support and all-over comfort. '
                  }
                }
              ],
              "stores": [
                'Nike Copacabana',
                'Nike Norte Shopping',
                'Nike Ipanema',
                'Nike RioSul Shopping',
                'Nike Barra Shopping',
                'Nike Leblon',
                'Artwalk RioSul shopping',
                'Authentic Feet Riosul shopping',
                'Authentic Feet Barra Shopping'
              ],
              "eCommerce" : "gonike.me/comprarAirMax",
              "video": "ultra-flyknit.webm",
              "qrcode": "qr-airmax.jpg",
              "video-frame": 'airmax.jpg'
            }
          ],
          "gallery": null,
          "active": true,
          "letterings": "sportswear.json"
        }
      ],
      "common": {
        "discover": {
          "pt-br": "Conheça",
          "en-us": "Discover"
        },
        "view": {
          "pt-br": "Ver Produto",
          "en-us": "View Product"
        },
        "learn-more": {
          "pt-br": "Saiba mais",
          "en-us": "Learn More"
        },
        "buy": {
          "pt-br": "comprar",
          "en-us": "SHOP NOW"
        }
      }
    }
  }
}

export { STATE };
