import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';

import { menuActions, internalActions, homeActions } from '../actions/index';
import HomeComponent from '../components/home';
import StoresModalComponent from '../components/stores-modal';

export default class Home extends Component {
  render() {
    return (
      <section>
        <StoresModalComponent />
        <HomeComponent />
      </section>
    )
  }
}

// --------------------------------------------------------------------
// get the properties from global state and send to component
// --------------------------------------------------------------------

function mapStateToProps(state) {
  return {
    selected : state.menu.selected.item,
    language : state.app.language,
    appStatus: state.app.initialized,
    enter    : state.home.enter,
    out      : state.home.out,
    resetInterval: state.home.resetInterval
  };
}

// --------------------------------------------------------------------
// Register the actions to components
// --------------------------------------------------------------------

const mapDispatchToProps = (dispatch) => {
  return {
    initialize: () => dispatch(menuActions.select('running')),
    openSectionDetail: () => {
      dispatch(homeActions.clearInterval())
      dispatch(internalActions.open())
    },
    swipeLeft: () => dispatch(homeActions.swipeLeft()),
    swipeRight: () => dispatch(homeActions.swipeRight()),
    autoPlay: () => dispatch(homeActions.autoPlay())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(HomeComponent);

