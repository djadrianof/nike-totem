import React, { Component, PropTypes } from 'react';
import { connect, bindActionCreators } from 'react-redux';
import { appActions } from '../actions/index';
import AppComponent from '../components/app';

export default class App extends Component {
  render() {
    return (
      <AppComponent />
    )
  }
}

// --------------------------------------------------------------------
// get the properties from global state and send to component
// --------------------------------------------------------------------

function mapStateToProps(state) {
  return {
    initialized: state.app.initialized,
    language: state.app.language
  };
}

// --------------------------------------------------------------------
// Register the actions to components
// --------------------------------------------------------------------

const mapDispatchToProps = (dispatch) => {
  return {}
}

export default connect(mapStateToProps, mapDispatchToProps)(AppComponent);

