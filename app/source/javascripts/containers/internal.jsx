import React, { Component, PropTypes } from 'react';
import { connect, bindActionCreators } from 'react-redux';
import InternalComponent from '../components/internal';

export default class Internal extends Component {
  render() {
    return (
      <InternalComponent />
    )
  }
}

// --------------------------------------------------------------------
// get the properties from global state and send to component
// --------------------------------------------------------------------

const mapStateToProps = (state) => {
  return {
    selected: state.menu.selected.item
  };
}

// --------------------------------------------------------------------
// Register the actions to components
// --------------------------------------------------------------------

const mapDispatchToProps = (dispatch) => {
  return {

  }
}

export default connect(mapStateToProps, mapDispatchToProps)(InternalComponent);

