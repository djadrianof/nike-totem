import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { tooltipActions } from '../actions/index';
import TooltipComponent from '../components/tooltip';

export default class Tooltip extends Component {
  render() {
    return (
      <TooltipComponent />
    )
  }
}

// --------------------------------------------------------------------
// get the properties from global state and send to component
// --------------------------------------------------------------------

function mapStateToProps(state) {
  return {
    language: state.app.language
  };
}

// --------------------------------------------------------------------
// Register the actions to components
// --------------------------------------------------------------------

const mapDispatchToProps = (dispatch) => {
  return {}
}

export default connect(mapStateToProps, mapDispatchToProps)(Tooltip);
export default connect(mapStateToProps, mapDispatchToProps)(TooltipComponent);

