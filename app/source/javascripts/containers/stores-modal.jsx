import React, { Component, PropTypes } from 'react';
import { connect, bindActionCreators } from 'react-redux';
import { storesModalActions, galleryActions, galleryDetailsModalActions } from '../actions/index';
import StoresModalComponent from '../components/stores-modal';

class StoresModal extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <StoresModalComponent />
    )
  }
}

// --------------------------------------------------------------------
// get the properties from global state and send to component
// --------------------------------------------------------------------

function mapStateToProps(state) {
  return {
    open: state.storesModal.open,
    section: state.menu.selected.item,
    product: state.products.slideUpdated.index,
    language : state.app.language,
  };
}

// --------------------------------------------------------------------
// Register the actions to components
// --------------------------------------------------------------------

const mapDispatchToProps = (dispatch) => {
  return {
    closeModal: () => dispatch(storesModalActions.close())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(StoresModalComponent);
