import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';

import { appActions } from '../actions/index';
import PreHomeComponent from '../components/pre-home';

export default class PreHome extends Component {
  render() {
    return (
      <PreHomeComponent />
    )
  }
}

// --------------------------------------------------------------------
// get the properties from global state and send to component
// --------------------------------------------------------------------

function mapStateToProps(state) {
  return {
    languageSelected: state.app.language
  }
}

// --------------------------------------------------------------------
// Register the actions to components
// --------------------------------------------------------------------

const mapDispatchToProps = (dispatch) => {
  return {
    onSelectLanguage: ( languageSelected ) => {
      dispatch(appActions.language(languageSelected));
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(PreHomeComponent);

