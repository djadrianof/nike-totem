import React, { Component, PropTypes } from 'react';
import { connect, bindActionCreators } from 'react-redux';
import { galleryDetailsModalActions, storesModalActions, galleryActions, galleryDetailsActions, productsActions } from '../../actions/index';
import ProductsComponent from '../../components/sections/products';

class Products extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <ProductsComponent />
    )
  }
}

// --------------------------------------------------------------------
// get the properties from global state and send to component
// --------------------------------------------------------------------

function mapStateToProps(state) {
  return {
    section     : state.menu.selected.item,
    open        : state.products.open,
    language    : state.app.language,
    slickCreated: state.products.slick,
    slideUpdated: state.products.slideUpdated.index
  };
}

// --------------------------------------------------------------------
// Register the actions to components
// --------------------------------------------------------------------

const mapDispatchToProps = (dispatch) => {
  return {
    changedSlide: (slide) => {
      dispatch(productsActions.changeSlide(slide));
    },
    slickInitialized: () => {
      dispatch(productsActions.slickInitialized());
    },
    openGallery: (evt) => {
      dispatch(galleryDetailsModalActions.open(evt));
    },
    closeProducts: () => {
      dispatch(productsActions.close());
    },
    openStores: () => {
      dispatch(storesModalActions.open());
    },
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ProductsComponent);
