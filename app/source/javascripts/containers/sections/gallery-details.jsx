import React, { Component, PropTypes } from 'react';
import { connect, bindActionCreators } from 'react-redux';
import { galleryDetailsModalActions } from '../../actions/index';
import GalleryDetailsComponent from '../../components/sections/gallery-details';

class GalleryDetails extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <GalleryDetailsComponent />
    )
  }
}

// --------------------------------------------------------------------
// get the properties from global state and send to component
// --------------------------------------------------------------------

function mapStateToProps(state) {
  return {
    section : state.gallery.section,
    images  : state.gallery.data,
    detail  : state.galleryDetailsModal.detail,
    modalOpened: state.galleryDetailsModal.open,
    language: state.app.language
  };
}

// --------------------------------------------------------------------
// Register the actions to components
// --------------------------------------------------------------------

const mapDispatchToProps = (dispatch) => {
  return {
    showDetail: ( evt ) => {
      let imageIndex = evt.currentTarget.getAttribute('data-image-index');
      dispatch(galleryDetailsModalActions.changeDetail(imageIndex));
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(GalleryDetailsComponent);
