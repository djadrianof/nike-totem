import React, { Component, PropTypes } from 'react';
import { connect, bindActionCreators } from 'react-redux';
import { selectMenu, startApp } from '../../actions/app';
import ViewerComponent from '../../components/home/menu';

class Viewer extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <ViewerComponent />
    )
  }
}

// --------------------------------------------------------------------
// get the properties from global state and send to component
// --------------------------------------------------------------------

function mapStateToProps(state) {
  return {
    section: state.menu.selected.item
  };
}

// --------------------------------------------------------------------
// Register the actions to components
// --------------------------------------------------------------------

const mapDispatchToProps = (dispatch) => {
  return {}
}

export default connect(mapStateToProps, mapDispatchToProps)(ViewerComponent);
