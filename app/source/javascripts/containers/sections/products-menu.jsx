import React, { Component, PropTypes } from 'react';
import { connect, bindActionCreators } from 'react-redux';
import { selectMenu, startApp } from '../../actions/app';
import ProductsMenuComponent from '../../components/sections/products-menu';

class ProductsMenu extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <ProductsMenuComponent />
    )
  }
}

// --------------------------------------------------------------------
// get the properties from global state and send to component
// --------------------------------------------------------------------

function mapStateToProps(state) {
  return {
    section     : state.menu.selected.item,
    selected    : state.productsMenu.selected,
    slideUpdated: state.products.slideUpdated.index,
    productOpened: state.products.open,
    language    : state.app.language
  };
}

// --------------------------------------------------------------------
// Register the actions to components
// --------------------------------------------------------------------

const mapDispatchToProps = (dispatch) => {
  return {}
}

export default connect(mapStateToProps, mapDispatchToProps)(ProductsMenuComponent);
