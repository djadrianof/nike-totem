import React, { Component, PropTypes } from 'react';
import { connect, bindActionCreators } from 'react-redux';
import { galleryActions, galleryDetailsModalActions } from '../../actions/index';
import GalleryComponent from '../../components/sections/gallery';

class Gallery extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <GalleryComponent />
    )
  }
}

// --------------------------------------------------------------------
// get the properties from global state and send to component
// --------------------------------------------------------------------

function mapStateToProps(state) {
  return {
    section : state.gallery.section,
    data    : state.gallery.data,
    loaded  : state.gallery.loaded,
    modalOpened: state.galleryDetailsModal.open,
    open    : state.gallery.open,
    language: state.app.language
  };
}

// --------------------------------------------------------------------
// Register the actions to components
// --------------------------------------------------------------------

const mapDispatchToProps = (dispatch) => {
  return {
    closeGallery: () => {
      dispatch(galleryActions.close());
    },
    openGalleryDetailsModal: () => {
      dispatch(galleryDetailsModalActions.open(0));
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(GalleryComponent);
