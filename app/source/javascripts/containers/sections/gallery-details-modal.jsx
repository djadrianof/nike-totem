import React, { Component, PropTypes } from 'react';
import { connect, bindActionCreators } from 'react-redux';
import { galleryDetailsModalActions } from '../../actions/index';
import GalleryDetailsModalComponent from '../../components/sections/gallery-details-modal';

class GalleryDetailsModal extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <GalleryDetailsModalComponent />
    )
  }
}

// --------------------------------------------------------------------
// get the properties from global state and send to component
// --------------------------------------------------------------------

function mapStateToProps(state) {
  return {
    section : state.gallery.section,
    images  : state.gallery.data,
    open    : state.galleryDetailsModal.open,
    detail  : state.galleryDetailsModal.detail,
    language: state.app.language
  };
}

// --------------------------------------------------------------------
// Register the actions to components
// --------------------------------------------------------------------

const mapDispatchToProps = (dispatch) => {
  return {
    closeGalleryDetailModal: () => dispatch(galleryDetailsModalActions.close()),
    changeGalleryDetailModal: ( detail ) => {
      dispatch(galleryDetailsModalActions.changeDetail( detail ))
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(GalleryDetailsModalComponent);
