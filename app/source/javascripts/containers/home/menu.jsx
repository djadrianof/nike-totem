import React, { Component, PropTypes } from 'react';
import { connect, bindActionCreators } from 'react-redux';
import { appActions, menuActions, homeActions } from '../../actions/index';
import MenuComponent from '../../components/home/menu';

class Menu extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <MenuComponent />
    )
  }
}

// --------------------------------------------------------------------
// get the properties from global state and send to component
// --------------------------------------------------------------------

function mapStateToProps(state) {
  return {
    selected: state.menu.selected.item,
    sections: state.menu.sections,
    language: state.app.language
  };
}

// --------------------------------------------------------------------
// Register the actions to components
// --------------------------------------------------------------------

const mapDispatchToProps = (dispatch) => {
  return {
    onSelectMenu: ( evt ) => {
      let itemSelected = evt.currentTarget.getAttribute('data-section-name');
      dispatch(menuActions.select(itemSelected));
      dispatch(homeActions.clearInterval());
    },
    onSelectLanguage: ( languageSelected ) => {
      dispatch(appActions.language(languageSelected));
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(MenuComponent);
