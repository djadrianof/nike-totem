import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import configureStore from './stores/configure';
import App from './containers/app';

import AppStyle from '../stylesheets/app';

const app = document.getElementById('nkTotem');

const store = configureStore();

render(
  <Provider store={store}>
    <App />
  </Provider>,
  app
);
