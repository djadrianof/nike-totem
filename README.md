# Nike - Olimpic BigScreen




## Environment


Adicione no hosts:
```
127.0.0.1            bigscreen.dev
192.168.99.100       bigscreen.dev
```



Cria a imagem:
```
docker build -t rgasp/nike-olimpic-bigscreen:1.0 .
```




Inicia o serviço web:
```
docker run -p 80:80 -p 8080:8080 -v `pwd`:/app rgasp/nike-olimpic-bigscreen:1.0 bash -c "npm install; npm start"
```
URL: <http://bigscreen.dev:8080/>



Construir a distribuição:
```
docker run -v `pwd`:/app rgasp/nike-olimpic-bigscreen:1.0 bash -c "npm run build"
```



Produção:
```
docker run -p 80:80 -v `pwd`:/app rgasp/nike-olimpic-bigscreen:1.0
```
URL: <http://bigscreen.dev/>





## Extra 

```
docker rm $(docker ps -a -q) -f
```

